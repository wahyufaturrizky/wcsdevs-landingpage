const Footer = (props) => (
	<footer id='footer'>
		<div className='inner'>
			<ul className='icons'>
				<li>
					<a
						href='https://twitter.com/wbocahbijak1'
						target='_blank'
						className='icon alt fa-twitter'>
						<span className='label'>Twitter</span>
					</a>
				</li>
				<li>
					<a
						href='https://www.facebook.com/wahyu.faturrizky/'
						target='_blank'
						className='icon alt fa-facebook'>
						<span className='label'>Facebook</span>
					</a>
				</li>
				<li>
					<a
						href='https://www.instagram.com/wahyufaturrizky/'
						target='_blank'
						className='icon alt fa-instagram'>
						<span className='label'>Instagram</span>
					</a>
				</li>
				<li>
					<a
						href='https://gitlab.com/wahyufaturrizky'
						target='_blank'
						className='icon alt fa-gitlab'>
						<span className='label'>Gitlab</span>
					</a>
				</li>
				<li>
					<a
						href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
						target='_blank'
						className='icon alt fa-linkedin'>
						<span className='label'>LinkedIn</span>
					</a>
				</li>
			</ul>
			<ul className='copyright'>
				<li>&copy; WAHYU FATUR RIZKI</li>
				<li>
					Copyright: <a href='#'>2021</a>
				</li>
			</ul>
		</div>
	</footer>
);

export default Footer;
