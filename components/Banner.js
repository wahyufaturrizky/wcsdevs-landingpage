const Banner = (props) => (
  <section id="banner" className="major">
    <div className="inner">
      <header className="major">
        <h1>
          Hello my name Wahyu as Full Stack Developer as Professional Consultant
          IT, Develop Web, Mobile Android and iOS
        </h1>
        <h3>
          We build ERP, POS, E-Commerce, B2B, Peer to Peer Landing, Accounting
          Sercive System
        </h3>
      </header>
      <div className="content">
        <p>Want to scale up your business, let's talk with Professional</p>
        <ul className="actions">
          <li>
            <a href="#one" className="button next scrolly">
              Get Started
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>
);

export default Banner;
