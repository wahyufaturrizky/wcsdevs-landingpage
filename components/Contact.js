import React, { useState } from "react";
const Contact = (props) => {
  const [state, setState] = useState({
    name: null,
    email: null,
    message: null,
  });
  const { name, email, message } = state;

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };
  const hanleSubmitMessage = () => {
    if (name && email && message) {
      window.open(
        `https://wa.me/6282274586011?text=Hello Mr. Wahyu Im interisting working with you, do you have time to us for discuss? \nmy name is ${name} (${email}) ${message}`,
        "_blank"
      );
    } else {
      window.alert("name email and message must be filling!");
    }
  };

  return (
    <section id="contact">
      <div className="inner">
        <section>
          <form
          // method='post' action='#'
          >
            <div className="field half first">
              <label htmlFor="name">Name</label>
              <input
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                type="text"
                name="name"
                id="name"
              />
            </div>
            <div className="field half">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                name="email"
                id="email"
              />
            </div>
            <div className="field">
              <label htmlFor="message">Message</label>
              <textarea
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                name="message"
                id="message"
                rows="6"
              ></textarea>
            </div>
            <ul className="actions">
              <li>
                <input
                  type="submit"
                  value="Send Message"
                  onClick={hanleSubmitMessage}
                  className="special"
                />
              </li>
              <li>
                <input type="reset" value="Clear" />
              </li>
            </ul>
          </form>
        </section>
        <section className="split">
          <section>
            <div className="contact-method">
              <span className="icon alt fa-envelope"></span>
              <h3>Email</h3>
              <a
                target="_blank"
                href="mailto:wahyufaturrizkyy@gmail.com?subject=Hello WAHYU DEV calling for Collaboration and Business&cc=dwityanafa@gmail.com&body=Hello Mr. Wahyu Im interisting working with you, do you have time to us for discuss?"
              >
                wahyufaturrizkyy@gmail.com
              </a>
            </div>
          </section>
          <section>
            <div className="contact-method">
              <span className="icon alt fa-phone"></span>
              <h3>Phone / Whatsapp</h3>
              <span>
                <a
                  target="_blank"
                  href="https://wa.me/6282274586011?text=Hello Mr. Wahyu Im interisting working with you, do you have time to us for discuss?"
                >
                  +62 822 7458 6011
                </a>
              </span>
            </div>
          </section>
          <section>
            <div className="contact-method">
              <span className="icon alt fa-home"></span>
              <h3>Address</h3>
              <span>
                <a href="https://goo.gl/maps/aYgUXmjJ1CbGD5c19" target="_blank">
                  Gg. Madirsan Ujung, Limau Manis,
                  <br />
                  Kabupaten Deli Serdang, Kecamatan Tanjung Morawa,
                  <br />
                  20362, Indonesia
                </a>
              </span>
            </div>
          </section>
        </section>
      </div>
    </section>
  );
};

export default Contact;
