import Link from 'next/link';

import Layout from '../components/Layout';
import Banner from '../components/Banner';

export default function Home() {
	return (
		<Layout>
			<div>
				<Banner />

				<div id='main'>
					<section id='one' className='tiles'>
						<article
							style={{ backgroundImage: `url('/static/images/pic01.jpg')` }}>
							<header className='major'>
								<h3>Beri Crowdfunding App</h3>
								<p>PT Senang Memberi</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.beri.mobileberiandroid.app'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic02.jpg')` }}>
							<header className='major'>
								<h3>PT Senang Memberi</h3>
								<p>Prototype application</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.beri.mobileberiandroid.app'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic03.jpg')` }}>
							<header className='major'>
								<h3>Pte Ltd Whatshalal</h3>
								<p>Prototype application</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.nurindustries.whatshalal'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic04.jpg')` }}>
							<header className='major'>
								<h3>ATEMS SYSTEM HALAL</h3>
								<p>Pte Ltd Whatshalal</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.nurindustries.whatshalal'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic05.jpg')` }}>
							<header className='major'>
								<h3>Cling App</h3>
								<p>Socialita App Fundrising</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.murahhati.cling'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic06.jpg')` }}>
							<header className='major'>
								<h3>PT Aplikasi Kemurahan Hati Indonesia</h3>
								<p>Prototype application</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.murahhati.cling'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic11.webp')` }}>
							<header className='major'>
								<h3>FWD Life Insurance Company</h3>
								<p>FWD Moment App</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=hk.fwd.moments'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic12.webp')` }}>
							<header className='major'>
								<h3>
									FWD Moments is creating new experiences to enhance engagement
								</h3>
								<p>
									with our customers and change the way people feel about
									insurance.n
								</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=hk.fwd.moments'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic13.webp')` }}>
							<header className='major'>
								<h3>SUN Education Group</h3>
								<p>Prototype application</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.sunedu.app'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic14.jpg')` }}>
							<header className='major'>
								<h3>SUN Education</h3>
								<p>APLIKASI KONSULTASI PENDIDIKAN TERLENGKAP di Indonesia</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.sunedu.app'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic15.jpg')` }}>
							<header className='major'>
								<h3>Deplaza ID</h3>
								<p>Prototype application</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.versiondeplaza'
								target='_blank'
								className='link primary'
							/>
						</article>
						<article
							style={{ backgroundImage: `url('/static/images/pic16.jpg')` }}>
							<header className='major'>
								<h3>Deplaza</h3>
								<p>APLIKASI RESELLER NO. 1 INDONESIA</p>
							</header>
							<a
								href='https://play.google.com/store/apps/details?id=com.versiondeplaza'
								target='_blank'
								className='link primary'
							/>
						</article>
					</section>
					<section id='two'>
						<div className='inner'>
							<ul className='actions'>
								<li>
									<a
										href='https://gitlab.com/wahyufaturrizky'
										target='_blank'
										className='button next'>
										More Projects
									</a>
								</li>
							</ul>
							<header className='major'>
								<h2>About Me</h2>
							</header>
							<p>
								Introduce my name is Wahyu Fatur Rizky, I'm a passionate Front
								End Developer & UI/UX Designer with more than 5 years of
								experience with React Native and Flutter, and etc, All my
								projects :
								<span>
									<a href='https://gitlab.com/wahyufaturrizky' target='_blank'>
										bit.ly/gitlabwahyu
									</a>
								</span>
								<span>
									{' '}
									my design & prototype{' '}
									<a
										href='https://dribbble.com/wahyu_faturrizky'
										target='_blank'>
										bit.ly/DribbbleWahyu
									</a>
								</span>
								<span>
									{' '}
									and my Profile{' '}
									<a
										href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
										target='_blank'>
										bit.ly/profilwahyu
									</a>
								</span>
								.
							</p>
						</div>
					</section>
				</div>
			</div>
		</Layout>
	);
}
